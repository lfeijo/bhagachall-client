package lucasfeijo;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.Scanner;

public class Main {

    static final int JOGADOR_CABRAS = 1;
    static final int JOGADOR_TIGRES = 2;

    static Scanner scanner;
    static Server obj;

    static int myId = 0;
    static int myRole = -1;
    static String oponente = null;
    static int cabras = 0;

    public static void main(String[] args) {

        try {
            obj = (Server) Naming.lookup( "//" +
                    "localhost:2000" +
                    "/BhagaChall");

            System.out.println("Insira um nome de usuario: ");
            scanner = new Scanner(System.in);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                int id = obj.registraJogador(line);
                if (id>=0) {
                    myId = id;
                    break;
                } else {
                    System.out.println("Erro: "+id+". Tente novamente: ");
                }
            }
            System.out.println("Seu id: "+myId+".");

            int temPartida = 0;
            while (temPartida==0) {
                System.out.println("Esperando partida...");
                Thread.sleep(1000);
                temPartida = (obj.temPartida(myId));
            }
            if (temPartida==-2) {
                System.out.println("Timeout");
                return; // exit
            } else if (temPartida==-1) { //
                Main.exit("Erro ao iniciar uma partida: "+temPartida);
            } else {
                myRole = temPartida;
            }
            System.out.println("Ok, você comanda "+(myRole==JOGADOR_CABRAS?"Cabras":"Tigres")+".");

            int ehMinhaVez;
            while(true) {
                ehMinhaVez = obj.ehMinhaVez(myId);
                switch (ehMinhaVez) {
                    case -2: // ainda nao tem 2 jogadores
                        Thread.sleep(1000);
                        System.out.println("Esperando jogador...");
                        continue;
                    case -1: // id invalido
                        Main.exit("Erro ao continuar jogo: "+ehMinhaVez);
                    case 0: // nao eh a vez
                        Thread.sleep(1000);
                        System.out.println("Esperando jogada...");
                        break;
                    case 1: // minha vez
                        fazJogada();
                        break;
                    case 2: // VENCI
                        Main.exit("Voce venceu.");
                        break;
                    case 3: // PERDI
                        Main.exit("Voce perdeu.");
                        break;
                    case 4: // EMPATE
                        Main.exit("Houve um empate.");
                        break;
                    case 5: // oponente fugiu
                        Main.exit("Voce venceu pois o oponente fugiu.");
                        break;
                    case 6: // eu fugi
                        Main.exit("Voce saiu do jogo.");
                        break;
                }
            }

        }
        catch (Exception e) {
            System.out.println("HelloClient exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void exit(String error) {
        System.out.println(error);
        scanner.close();
        System.exit(0);
    }

    public static void fazJogada() throws RemoteException {
        String grade = obj.obtemGrade(myId);
        System.out.println(grade!=null?grade:"");

        if (myRole == JOGADOR_CABRAS) {
            if (cabras<20) {
                System.out.println("Adicione uma cabra");
                System.out.println("Coord x");
                String x = scanner.nextLine();
                System.out.println("Coord y");
                String y = scanner.nextLine();
                int v = obj.posicionaCabra(myId, new Integer(x), new Integer(y));
                if (v==1) cabras++;
                else System.out.println("Comando invalido");
            } else {
                System.out.println("Mova uma cabra");
                System.out.println("Nome");
                String x = scanner.nextLine();
                System.out.println("Direcao");
                String y = scanner.nextLine();
                int v = obj.moveCabra(myId, x.toCharArray()[0], new Integer(y));
                if (v==1) {}
                else System.out.println("Comando invalido");
            }

        } else if (myRole == JOGADOR_TIGRES) {
            System.out.println("Mova um tigre");
            String line = scanner.nextLine();



        }
    }
}
